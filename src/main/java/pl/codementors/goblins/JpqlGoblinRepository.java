package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class JpqlGoblinRepository implements GoblinRepository{

    @Override
    public Query findAllGoblins(EntityManager em) {
        return em.createQuery("select g from Goblin g");
    }

    @Override
    public Query findAllGoblinsWithoutWeapons(EntityManager em) {
        return em.createQuery("select g from Goblin g where g.weapon is empty");
    }

    @Override
    public Query deleteAllGoblins(EntityManager em) {
        return em.createQuery("delete from Goblin g");
    }

    @Override
    public Query findAllGoblinsTallerThan13(EntityManager em) {
        return em.createQuery("select g from Goblin g where g.height > 13");
    }
}
