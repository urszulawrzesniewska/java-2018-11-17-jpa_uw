package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.Scanner;

public class JPAMain {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("GoblinsPU");


    public static void main(String[] args) {

        GoblinRepository repository = new JpqlGoblinRepository();
        CaveRepository caveRepository = new JpqlCaveRepository();
        WeaponRepository weaponRepository = new JpqlWeaponRepository();


        GoblinDAO goblinDAO = new GoblinDAO(emf, repository);
        CaveDAO caveDAO = new CaveDAO(emf, caveRepository);
        WeaponDAO weaponDAO = new WeaponDAO(emf, weaponRepository);

        DAOMain daoMain = new DAOMain();

        Cave cave1 = new Cave(500);
        Cave cave2 = new Cave(1900);
        Cave cave3 = new Cave(2400);

        caveDAO.persist(cave1);
        caveDAO.persist(cave2);
        caveDAO.persist(cave3);

        Goblin goblin1 = new Goblin("Derd",13);
        Goblin goblin2 = new Goblin("Modiz",14);
        Goblin goblin3 = new Goblin("Kwika",12);
        Goblin goblin4 = new Goblin("Grimel",13);
        Goblin goblin5 = new Goblin("Givierd",14);
        Goblin goblin6 = new Goblin("Jetvixle",14);
        Goblin goblin7 = new Goblin("Riz",12);
        Goblin goblin8 = new Goblin("Kazmienk",15);

        goblinDAO.persist(goblin1);
        goblinDAO.persist(goblin2);
        goblinDAO.persist(goblin3);
        goblinDAO.persist(goblin4);
        goblinDAO.persist(goblin5);
        goblinDAO.persist(goblin6);
        goblinDAO.persist(goblin7);
        goblinDAO.persist(goblin8);


        Weapon weapon1 = new Weapon("Long Sword", 4, Weapon.Type.BLADE);
        Weapon weapon2 = new Weapon("Short Sword", 2, Weapon.Type.BLADE);
        Weapon weapon3 = new Weapon("Hammer", 3, Weapon.Type.BLUNT);
        Weapon weapon4 = new Weapon("Club", 3, Weapon.Type.BLUNT);
        Weapon weapon5 = new Weapon("Bronze spear", 1, Weapon.Type.STABBING);
        Weapon weapon6 = new Weapon("Adamant spear", 3, Weapon.Type.STABBING);
        Weapon weapon7 = new Weapon("War Hammer", 4, Weapon.Type.BLUNT);
        Weapon weapon8 = new Weapon("Two-handed Sword", 6, Weapon.Type.BLADE);

        weaponDAO.persist(weapon1);
        weaponDAO.persist(weapon2);
        weaponDAO.persist(weapon3);
        weaponDAO.persist(weapon4);
        weaponDAO.persist(weapon5);
        weaponDAO.persist(weapon6);
        weaponDAO.persist(weapon7);
        weaponDAO.persist(weapon8);


        weapon1.setGoblin(goblin1);
        weapon2.setGoblin(goblin2);
        weapon3.setGoblin(goblin3);
        weapon4.setGoblin(goblin4);
        weapon5.setGoblin(goblin5);
        weapon6.setGoblin(goblin6);

        weaponDAO.merge(weapon1);
        weaponDAO.merge(weapon2);
        weaponDAO.merge(weapon3);
        weaponDAO.merge(weapon4);
        weaponDAO.merge(weapon5);
        weaponDAO.merge(weapon6);

        goblin1.setWeapon(weapon1);
        goblin2.setWeapon(weapon2);
        goblin3.setWeapon(weapon3);
        goblin4.setWeapon(weapon4);
        goblin5.setWeapon(weapon5);
        goblin6.setWeapon(weapon6);

        goblinDAO.merge(goblin1);
        goblinDAO.merge(goblin2);
        goblinDAO.merge(goblin3);
        goblinDAO.merge(goblin4);
        goblinDAO.merge(goblin5);
        goblinDAO.merge(goblin6);



        goblin1.setCave(cave1);
        goblin2.setCave(cave1);
        goblin3.setCave(cave1);
        goblin4.setCave(cave2);
        goblin5.setCave(cave2);
        goblin6.setCave(cave2);
        goblin7.setCave(cave2);
        goblin8.setCave(cave2);

        goblinDAO.merge(goblin1);
        goblinDAO.merge(goblin2);
        goblinDAO.merge(goblin3);
        goblinDAO.merge(goblin4);
        goblinDAO.merge(goblin5);
        goblinDAO.merge(goblin6);
        goblinDAO.merge(goblin7);
        goblinDAO.merge(goblin8);


        while (true) {
            System.out.println("Choose action: ");
            System.out.println("1. List all goblins");
            System.out.println("2. List all caves");
            System.out.println("3. List all weapons");
            System.out.println("4. List weapons without goblins");
            System.out.println("5. List goblins without weapons");
            System.out.println("6. List all empty caves");
            System.out.println("7. List all goblins taller than 13");
            System.out.println("8. Delete all goblins");
            System.out.println("9. Delete all caves");
            System.out.println("10. Delete all weapons");
            System.out.println("11. Quit");


            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();

            if (choice.equals("1")) {
                System.out.println("List all goblins:");
                goblinDAO.findAll().forEach(System.out::println);
            }

            if (choice.equals("2")) {

                System.out.println("List all caves:");
                caveDAO.findAll().forEach(System.out::println);
            }

            if (choice.equals("3")) {
                System.out.println("List all weapons:");
                weaponDAO.findAll().forEach(System.out::println);
            }

            if (choice.equals("4")) {
                System.out.println("List weapons without goblins:");
                weaponDAO.findAllWeaponsWithoutGoblins().forEach(System.out::println);
            }
            if (choice.equals("5")) {
                System.out.println("List goblins without weapons:");
                goblinDAO.findAllGoblinsWithoutWeapons().forEach(System.out::println);
            }
            if (choice.equals("6")) {
                System.out.println("List all empty caves:");
                caveDAO.findAllEmptyCaves().forEach(System.out::println);
            }

            if (choice.equals("7")) {
                System.out.println("List all goblins taller than 13:");
                goblinDAO.findAllGoblinsTallerThan13().forEach(System.out::println);

            }
            if (choice.equals("8")) {
                System.out.println("Delete all goblins");
                goblinDAO.delete(goblin1);
                goblinDAO.delete(goblin2);
                goblinDAO.delete(goblin3);
                goblinDAO.delete(goblin4);
                goblinDAO.delete(goblin5);
                goblinDAO.delete(goblin6);
                goblinDAO.delete(goblin7);
                goblinDAO.delete(goblin8);

                goblinDAO.findAll().forEach(System.out::println);
            }
            if (choice.equals("9")) {
                System.out.println("Delete all caves");
                caveDAO.delete(cave1);
                caveDAO.delete(cave2);
                caveDAO.delete(cave3);

                caveDAO.findAll().forEach(System.out::println);
            }

            if (choice.equals("10")) {
                System.out.println("Delete all weapons");
                weaponDAO.delete();


                weaponDAO.findAll().forEach(System.out::println);
            }

            if (choice.equals("11")) {
                System.out.println("Quit");
                break;
            }

        }

        GoblinEntityManagerFactory.close();
        emf.close();
    }


}
