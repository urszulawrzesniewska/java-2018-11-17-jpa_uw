package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class JpqlWeaponRepository implements WeaponRepository{

    @Override
    public Query findAllWeapons(EntityManager em) {
        return em.createQuery("select w from Weapons w");
    }

    @Override
    public Query findAllWeaponsWithoutGoblins(EntityManager em) {
        return em.createQuery("select w from Weapon w where w.goblin is empty");
    }

    @Override
    public Query deleteAllWeapons(EntityManager em) {
        return em.createQuery("delete from Weapon w");
    }
}
