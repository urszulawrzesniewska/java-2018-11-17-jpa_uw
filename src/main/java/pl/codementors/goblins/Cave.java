package pl.codementors.goblins;


import org.hibernate.annotations.Cascade;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "cave")

public class Cave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int surface;

    @OneToMany(mappedBy = "cave", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private List<Goblin> goblinList;

    public Cave(int surface) {
        this.surface = surface;
    }

    public Cave(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public List<Goblin> getGoblinList() {
        return goblinList;
    }

    public void setGoblinList(List<Goblin> goblinList) {
        this.goblinList = goblinList;
    }

    @Override
    public String toString() {
        return "Cave{" +
                ", surface=" + surface +
                ", goblinList=" + goblinList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cave)) return false;
        Cave cave = (Cave) o;
        return id == cave.id &&
                surface == cave.surface &&
                Objects.equals(goblinList, cave.goblinList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, surface, goblinList);
    }
}