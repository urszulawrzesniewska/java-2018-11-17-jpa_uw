package pl.codementors.goblins;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "weapon")

public class Weapon {

    public enum Type {
        STABBING,
        BLADE,
        BLUNT;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private int damage;

    @Enumerated(EnumType.STRING)
    @Column
    private Type type;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "goblin", referencedColumnName = "id")
    private Goblin goblin;

    public Weapon(String name, int damage, Type type) {
        this.name = name;
        this.damage = damage;
        this.type = type;
    }
    public Weapon(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Goblin getGoblin() {
        return goblin;
    }

    public void setGoblin(Goblin goblin) {
        this.goblin = goblin;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                ", name='" + name + '\'' +
                ", damage=" + damage +
                ", type=" + type +
                ", goblin=" + goblin +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Weapon)) return false;
        Weapon weapon = (Weapon) o;
        return id == weapon.id &&
                damage == weapon.damage &&
                Objects.equals(name, weapon.name) &&
                type == weapon.type &&
                Objects.equals(goblin, weapon.goblin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, damage, type, goblin);
    }
}
