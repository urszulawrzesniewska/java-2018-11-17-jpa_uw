package pl.codementors.goblins;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "goblin")

public class Goblin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private int height;

    @ManyToOne
    @JoinColumn(name = "cave", referencedColumnName = "id")
    private Cave cave;

    @OneToOne(mappedBy = "goblin", cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "weapon", referencedColumnName = "id")
    private Weapon weapon;

    public Goblin(String name, int height) {
        this.name = name;
        this.height = height;

    }

    public Goblin(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Cave getCave() {
        return cave;
    }

    public void setCave(Cave cave) {
        this.cave = cave;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public String toString() {
        return "Goblin{" +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", cave=" + cave.getSurface() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Goblin)) return false;
        Goblin goblin = (Goblin) o;
        return id == goblin.id &&
                height == goblin.height &&
                Objects.equals(name, goblin.name) &&
                Objects.equals(cave, goblin.cave) &&
                Objects.equals(weapon, goblin.weapon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, height, cave, weapon);
    }
}
