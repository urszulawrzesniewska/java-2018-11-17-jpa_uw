package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class JpqlCaveRepository implements CaveRepository {

    @Override
    public Query findAllCaves(EntityManager em) {
        return em.createQuery("select c from Cave c");
    }

    @Override
    public Query findAllEmptyCaves(EntityManager em) {
        return em.createQuery("select c from Cave c where c.goblinList is empty");
    }

    @Override
    public Query deleteAllCaves(EntityManager em) {
        return em.createQuery("delete from Cave c");
    }
}
