package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public interface GoblinRepository {

    Query findAllGoblins(EntityManager em);

    Query findAllGoblinsWithoutWeapons(EntityManager em);

    Query deleteAllGoblins(EntityManager em);

    Query findAllGoblinsTallerThan13(EntityManager em);


}

