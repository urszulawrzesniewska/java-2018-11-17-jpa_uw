package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public interface WeaponRepository {

    Query findAllWeapons(EntityManager em);

    Query findAllWeaponsWithoutGoblins(EntityManager em);

    Query deleteAllWeapons(EntityManager em);

}
