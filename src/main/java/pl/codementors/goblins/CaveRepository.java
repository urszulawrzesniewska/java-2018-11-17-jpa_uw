package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public interface CaveRepository {

    Query findAllCaves(EntityManager em);

    Query findAllEmptyCaves(EntityManager em);

    Query deleteAllCaves(EntityManager em);
}
