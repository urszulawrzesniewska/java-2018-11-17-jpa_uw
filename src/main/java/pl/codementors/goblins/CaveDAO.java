package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class CaveDAO {

    private final EntityManagerFactory emf;

    private CaveRepository caveRepository;

    public CaveDAO(EntityManagerFactory emf, CaveRepository caveRepository) {
        this.emf = emf;
        this.caveRepository = caveRepository;
    }

    public Cave persist(Cave cave) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(cave);
        tx.commit();
        em.close();
        return cave;
    }
    public void delete(Cave cave) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.find(Cave.class,cave.getId()));
        tx.commit();
        em.close();

/*        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        caveRepository.deleteAllCaves(em).executeUpdate();
        em.getTransaction().commit();
        em.close();*/
    }

    public Cave merge(Cave cave) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        cave = em.merge(cave);
        tx.commit();
        em.close();
        return cave;
    }


    public List<Cave> findAll() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Cave> caves = em.createQuery("select c from Cave c").getResultList();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Cave> query = cb.createQuery(Cave.class);
        query.from(Cave.class);
        em.close();
        return caves;
    }
    public List<Cave> findAllEmptyCaves() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Cave> caves = em.createQuery("select c from Cave c where c.goblinList is empty").getResultList();
        em.close();
        return caves;
    }
  /*  public List<Cave> findAllCaves() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Cave> caves = caveRepository.findAllCaves(em).getResultList();
        em.close();
        return caves;
    }*/
}
