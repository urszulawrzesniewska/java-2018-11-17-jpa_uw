package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class WeaponDAO {


    private final EntityManagerFactory emf;

    private WeaponRepository weaponRepository;

    public WeaponDAO(EntityManagerFactory emf, WeaponRepository weaponRepository) {
        this.emf = emf;
        this.weaponRepository = weaponRepository;
    }
    public Weapon persist(Weapon weapon) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(weapon);
        tx.commit();
        em.close();
        return weapon;
    }
    public void delete() {
/*        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.find(Weapon.class,weapon.getId()));
        tx.commit();
        em.close();*/

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        weaponRepository.deleteAllWeapons(em).executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
    public Weapon merge(Weapon weapon) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        weapon = em.merge(weapon);
        tx.commit();
        em.close();
        return weapon;

    }

    public List<Weapon> findAll() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Weapon> weapons = em.createQuery("select w from Weapon w").getResultList();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Weapon> query = cb.createQuery(Weapon.class);
        query.from(Weapon.class);
        em.close();
        return weapons;
    }

    public List<Weapon> findAllWeaponsWithoutGoblins() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Weapon> weapons = em.createQuery("select w from Weapon w where w.goblin is empty").getResultList();
        em.close();
        return weapons;
    }

    /*public List<Weapon> findAllWeapons() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Weapon> weapons = weaponRepository.findAllWeapons(em).getResultList();
        em.close();
        return weapons;
    }*/
}
