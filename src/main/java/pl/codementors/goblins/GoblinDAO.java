package pl.codementors.goblins;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class GoblinDAO {

    private final EntityManagerFactory emf;

    private GoblinRepository repository;

    public GoblinDAO(EntityManagerFactory emf, GoblinRepository repository) {
        this.emf = emf;
        this.repository = repository;
    }

    public Goblin persist(Goblin goblin) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(goblin);
        tx.commit();
        em.close();
        return goblin;
    }
    public void delete(Goblin goblin) {

        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Goblin g = em.find(Goblin.class, goblin.getId());
        g.getCave().getGoblinList().remove(g);
        em.remove(g);
        tx.commit();
        em.close();

/*        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        repository.deleteAllGoblins(em).executeUpdate();
        em.getTransaction().commit();
        em.close();*/
    }

    public Goblin merge(Goblin goblin) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        goblin = em.merge(goblin);
        tx.commit();
        em.close();
        return goblin;
    }

    public List<Goblin> findAll() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Goblin> goblins = em.createQuery("select g from Goblin g").getResultList();
        em.close();
        return goblins;
    }

    public List<Goblin> findAllGoblinsWithoutWeapons() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Goblin> goblins = em.createQuery("select g from Goblin g where g.weapon is empty").getResultList();
        em.close();
        return goblins;
    }
    public List<Goblin> findAllGoblinsTallerThan13() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Goblin> goblins = em.createQuery("select g from Goblin g where g.height > 13").getResultList();
        em.close();
        return goblins;
    }

   /* public List<Goblin> findAllGoblins() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Goblin> goblins = repository.findAllGoblins(em).getResultList();
        em.close();
        return goblins;
    }*/
}
